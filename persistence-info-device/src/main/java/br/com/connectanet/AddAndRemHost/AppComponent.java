package br.com.connectanet.AddAndRemHost;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.onosproject.core.ApplicationId;
import org.onosproject.core.CoreService;
import org.onosproject.net.device.DeviceAdminService;
import org.onosproject.net.device.DeviceEvent;
import org.onosproject.net.device.DeviceListener;
import org.onosproject.net.device.DeviceService;
import org.onosproject.net.link.LinkEvent;
import org.onosproject.net.link.LinkListener;
import org.onosproject.net.link.LinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Skeletal ONOS application component.
 */
@Component(immediate = true)
public class AppComponent {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
	private CoreService coreService;

	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
	private DeviceService deviceService;

	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
	private LinkService linkService;

	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY)
	private DeviceAdminService deviceAdminService;

	private DeviceListener deviceServiceListener;
	private LinkServiceListener linkServiceListener;

	private ApplicationId applicationId;

	@Activate
	protected void activate() {
		log.info("Started");

		applicationId = this.coreService.registerApplication(this.getClass().getCanonicalName());

		deviceServiceListener = new DeviceServiceListener();
		linkServiceListener = new LinkServiceListener();

		deviceService.addListener(deviceServiceListener);
		linkService.addListener(linkServiceListener);
	}

	@Deactivate
	protected void deactivate() {
		log.info("Stopped");

		deviceService.removeListener(deviceServiceListener);
		linkService.removeListener(linkServiceListener);
	}

	private class DeviceServiceListener implements DeviceListener {

		@Override
		public void event(DeviceEvent DEnv) {
			switch (DEnv.type()) {
			case DEVICE_ADDED:
				log.info("------------------------------------------------------");
				log.info("AP Added ID ({})", DEnv.subject().id());
				log.info("AP Added Version ({})", DEnv.subject().swVersion());
				log.info("AP Added Serial Number ({})", DEnv.subject().serialNumber());
				log.info("AP Added Manufature ({})", DEnv.subject().manufacturer());
				log.info("AP Added Chassis ID ({})", DEnv.subject().chassisId());
				log.info("------------------------------------------------------");
				log.info("");
				break;

			case DEVICE_REMOVED:
				log.info("=============================================================");
				log.info("AP Removed ID ({})", DEnv.subject().id());
				log.info("AP Removed Version ({})", DEnv.subject().swVersion());
				log.info("AP Removed Serial Number ({})", DEnv.subject().serialNumber());
				log.info("AP Removed Manufature ({})", DEnv.subject().manufacturer());
				log.info("AP Removed Chassis ID ({})", DEnv.subject().chassisId());
				log.info("=============================================================");
				log.info("");
				break;

			case DEVICE_AVAILABILITY_CHANGED:
				log.info("");
				if (!deviceAdminService.isAvailable(DEnv.subject().id())) {
					deviceAdminService.removeDevice(DEnv.subject().id());
					log.info("Device Removed ID ({})", DEnv.subject().id().uri().toString());
					log.info("");
				}
				break;
			default:
				break;

			}
		}
	}

	class LinkServiceListener implements LinkListener {

		@Override
		public void event(LinkEvent LinkEnv) {

			switch (LinkEnv.type()) {
			case LINK_ADDED:
				log.info("------------------------------------------------------");
				log.info("Link Added IS Durable {}", LinkEnv.subject().isDurable());
				log.info("Link Added Dst ({})", LinkEnv.subject().dst().toString());
				log.info("Link Added SRC ({})", LinkEnv.subject().src().toString());
				log.info("Link Added State ({})", LinkEnv.subject().state().toString());
				log.info("Link Added Type ({})", LinkEnv.subject().type().toString());
				log.info("------------------------------------------------------");
				log.info("");
				break;
			case LINK_REMOVED:
				log.info("=============================================================");
				log.info("Link Removed IS Durable {}", LinkEnv.subject().isDurable());
				log.info("Link Removed Dst ({})", LinkEnv.subject().dst().toString());
				log.info("Link Removed SRC ({})", LinkEnv.subject().src().toString());
				log.info("Link Removed State ({})", LinkEnv.subject().state().toString());
				log.info("Link Removed Type ({})", LinkEnv.subject().type().toString());
				log.info("=============================================================");
				log.info("");
				break;
			default:
				break;

			}
		}
	}

	public void recebeinf(InformationDeviceGrep inforDespositivo) throws IOException {

		Path path = Paths.get("home/Documentos/texto.txt");
		Charset utf8 = StandardCharsets.UTF_8;
		//Files.newBufferedWriter(path, utf8);

		try (BufferedWriter w = Files.newBufferedWriter(path, utf8)) {
			w.write(inforDespositivo.getId() + ";" + inforDespositivo.getNumberSerial() +"\n");
			w.flush();

		} catch (IOException e) {
			e.printStackTrace() ;
		}
	}

	public class InformationDeviceGrep {

		public String id;
		public String serialNumber;

		private String getId() {
			return id;
		}

		private String getNumberSerial() {
			return serialNumber;
		}
	}
		
	// public void lerInf(InformationDeviceGrep inforDespositivo) {
	//
	// }
}
